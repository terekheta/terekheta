(ns terekheta.hello
  (:require [enfocus.core :as ef]
            [enfocus.events :as events]
        [enfocus.effects :as effects])
  (:require-macros [enfocus.macros :as em]))

(defn start []
  ) ;; (ef/at [:.container] (ef/content "Hello enfocus!"))

(set! (.-onload js/window) start)
