(ns terekheta.posts.attachments
  (:require [cognitect.transit :as transit]
            [ajax.core :refer [POST GET PUT]]
            [enfocus.core :as ef]
            [enfocus.events :as events]
            [enfocus.effects :as effects]
            [goog.dom :as gdom]
            [cljs.core.async :refer [>! <! chan close! put! take!]]
;;             [jayq.core :as jq :refer [$]]

            [terekheta.utils.send-form :refer [send-form-as-data]]

            [terekheta.attachments.core :as att]
            [terekheta.attachments.image-link :as img-link-att]
            [terekheta.attachments.image-upload :as image-upload-att]
            [terekheta.attachments.youtube-video :as youtube-video-att])
  (:require-macros [enfocus.macros :as em]
                   [cljs.core.async.macros :refer [go go-loop]]))


(def attachments (atom []))


(defn- on-add-attachment [type_ e]
  (.preventDefault e)
  (.log js/console "type_" type_)
  (.log js/console "event" e)
  (go
    (let [atts-el (.querySelector js/document ".post__attachments")
          att (<! (att/add-attachment type_ atts-el e))]  ;; wait for user actions
      (.log js/console "att:" (pr-str att))
      (when att
        (swap! attachments conj att)  ;; add attachment
        (<! (att/add-attachment-edit-preview att atts-el))  ;; wait for remove
        (swap! attachments #(->> % (remove #{att}) vec))))))  ;; remove attachment


(defn- submit-post [e]
  (.preventDefault e)
  (let [post-form (.querySelector js/document ".post-edit")]
    (.log js/console @attachments)
    (send-form-as-data post-form {:attachments @attachments})))


(em/defaction setup []
  [".post-edit"] (events/listen :submit submit-post)

  [".attachment__add-youtube-video"] (events/listen :click (partial on-add-attachment :youtube-video))
  [".attachment__add-image-link"] (events/listen :click (partial on-add-attachment :image-link))
  [".attachment__add-image-file"] (events/listen :click #(-> js/document ;; TODO get parent div
                                                             (.querySelector ".attachment__image-upload")
                                                             .click))
  [".attachment__image-upload"] (events/listen :change (partial on-add-attachment :image-upload)))

;; TODO move to component
(defn start []
  ;;   (.log js/console "HI!" js/post_bootstrap)
  (let [reader (transit/reader :json {:handlers { "r" (fn [x] x) }}) ;; override ?BAD? url type handler
        post (transit/read reader js/post_bootstrap)
        atts (:attachments post)
        atts-el (.querySelector js/document ".post__attachments")]  ;; (.log js/console "HI!" (pr-str ))
    (.log js/console (pr-str (str (:url (first atts)))))
    (doseq [att atts]
      (swap! attachments conj att)  ;; add attachment
      (go
       (<! (att/add-attachment-edit-preview att atts-el))  ;; wait for remove
       (swap! attachments #(->> % (remove (fn [a] (= a att))) vec))))
    (.log js/console "HI!" (pr-str @attachments)))
  (setup))

;; (defn start []
;;   (ef/at [:.container] (ef/content "Hello enfocus2!")))

(set! (.-onload js/window) start)
