(ns terekheta.attachments.image-upload
  (:require [ajax.core :refer [POST GET]]
            [goog.events :as gev]

            [enfocus.core :as ef]
            [enfocus.events :as events]
            [enfocus.effects :as effects]
            [cljs.core.async :refer [>! <! chan close! put! take!]]

            [terekheta.attachments.core :refer [attachment-edit attachment-edit-init]]
            [terekheta.attachments.image-core :refer [image-ext? image-type? image-attachment-preview]])
  (:require-macros [enfocus.macros :as em]
                   [cljs.core.async.macros :refer [go go-loop]]))


(defn on-success [ch response]
;;   (.log js/console "success!" (pr-str response))
  (put! ch response))

(defn on-error [ch response]
  (.log js/console "error!" (pr-str response))
;;   (set! (-> js/document
;;             (.querySelector "main")
;;             .-innerHTML)

;;         (+ (-> js/document
;;             (.querySelector "main")
;;             .-innerHTML)
;;            (response :response)))
  (close! ch))

(defn on-progress [e]
  (.log js/console "progress!" e)
  (when-let [progress (/ (.-loaded e) (.-total e))]
    (.log js/console "progress!" progress)))


(em/defsnippet image-upload-attachment-edit :compiled "templates/blog.html" [".attachment__image-upload-edit"]
  [])


(defmethod attachment-edit :image-upload [type_ ch e]
  (.log js/console "image upload!" e)
;;   (image-upload-attachment-edit ch))
  (let [file-input (.-currentTarget e)
        file (first (array-seq (.-files file-input)))
        filename (.-name file)]
    (.log js/console "file:" file)
    (if (and (image-ext? filename) (image-type? file))
      (let [form-data (doto (js/FormData.)
                        (.append "file" file)
                        (.append "__anti-forgery-token" (-> js/document (.querySelector "[name=csrf-token]") .-content)))

            xhrio (POST "/upload" {:params form-data
                                   :handler (partial on-success ch)
                                   :error-handler (partial on-error ch)})]
        ;;             DOES NOT WORK:
        ;;             (.listen xhrio goog.net.EventType/PROGRESS on-progress)
        (image-upload-attachment-edit))
      (js/alert "invalid file!"))))

