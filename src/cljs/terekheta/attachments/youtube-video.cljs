(ns terekheta.attachments.youtube-video
  (:require [enfocus.core :as ef]
            [enfocus.events :as events]
            [enfocus.effects :as effects]
            [goog.dom :as gdom]
            [cljs.core.async :refer [>! <! chan close! put! take!]]

            [terekheta.attachments.core :refer [attachment-edit attachment-preview]]
            ) ;; [jayq.core :as jq :refer [$]]
  (:require-macros [enfocus.macros :as em]
                   [cljs.core.async.macros :refer [go go-loop]]))


(def youtube-video-regex (js/RegExp. "^.*(youtu.be\\/|v\\/|u\\/\\w\\/|embed\\/|watch\\?v=|\\&v=)([^#\\&\\?]{11}).*"))

(defn- get-youtube-video-id [url]
  (let [match (.exec youtube-video-regex url)
        part (second match)
        id (last match)]
    (when (and part id)
      id)))

(em/defsnippet youtube-video-attachment :compiled "templates/_.html" [".ytplayer"]
  [att]

  ".ytplayer"
    (ef/replace-vars att))


(em/defsnippet youtube-video-thumbnail :compiled "templates/_.html" [".ytthumb"]
  [att]

  ".ytthumb"
    (ef/replace-vars att))


(em/defsnippet youtube-video-attachment-edit :compiled "templates/blog.html" [".attachment__youtube-video-edit"]
  [ch link]

  ".attachment__youtube-video-edit"
    (events/listen :focusout
       (fn [e]
;;          (.log js/console "focusout!")
         (close! ch)))

  ".attachment__youtube-video-edit__link-input"
    (ef/set-attr :value link)

  ".attachment__youtube-video-edit__confirm"
    (events/listen :mousedown
      (fn [e]
        (.preventDefault e)
;;         (.log js/console "mousedown")
        (let [link (clojure.string/trim (ef/from ".attachment__youtube-video-edit__link-input" (ef/get-prop :value)))]
          (when-let [video-id (get-youtube-video-id link)]
            (put! ch {:type :youtube-video :id video-id}))))))


(em/defsnippet youtube-video-attachment-preview :compiled "templates/blog.html" [".attachment__youtube-video-preview"]
  [att]

  ".attachment__youtube-video-preview"
    (ef/replace-vars att))


(defmethod attachment-edit :youtube-video [type_ ch event link]
  (.log js/console "youtube video add attachment!")
  (youtube-video-attachment-edit ch link))

(defmethod attachment-preview :youtube-video [att]
  (.log js/console "youtube attachment preview!")
  (youtube-video-attachment-preview att))

