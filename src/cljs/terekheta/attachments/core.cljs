(ns terekheta.attachments.core
  (:require [cljs.core.async :refer [<! chan close!]]
            [terekheta.attachments.core-templates :refer [attachment-edit-preview]])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))


(defmulti attachment-edit (fn [type_ ch event & params] type_))
(defmulti attachment-edit-init (fn [type_ el & params] type_))
(defmulti attachment-edit-clean (fn [type_ el & params] type_))
(defmulti attachment-preview (fn [att] (att :type)))

(defmethod attachment-edit :default [type_ ch & params]
  (close! ch))

(defmethod attachment-preview :default [att] "NO TEMPLATE!")

(defmethod attachment-edit-init :default [type_ el att]
  (when-let [url-input (.querySelector el "input")]
    (.focus url-input)))

(defmethod attachment-edit-clean :default [type_ el att]
  )


(defn add-attachment [type_ atts-el e]
  (let [ch (chan)
        el (-> (attachment-edit type_ ch e) .-firstElementChild)]
    (.insertBefore atts-el el (.-firstChild atts-el))
    (attachment-edit-init type_ el)
    (go
      (let [att (<! ch)]  ;; block and wait here
        (.log js/console "att" (pr-str att) att)
        (attachment-edit-clean type_ el)
        (.removeChild atts-el el)
        att))))


(defn add-attachment-edit-preview [att atts-el]
  (let [delete-listener (chan)
        content (-> (attachment-preview att) .-firstElementChild .-innerHTML)

        el (-> (attachment-edit-preview delete-listener content) .-firstElementChild)]
    (.appendChild atts-el el)
    (go
      (<! delete-listener)  ;; block and wait for delete
      (.removeChild atts-el el))))
