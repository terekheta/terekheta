(ns terekheta.attachments.image-link
  (:require [enfocus.core :as ef]
            [enfocus.events :as events]
            [enfocus.effects :as effects]
            [cljs.core.async :refer [>! <! chan close! put! take!]]

            [terekheta.attachments.core :refer [attachment-edit attachment-preview]]
            [terekheta.attachments.image-core :refer [image-ext? image-attachment-preview]])
  (:require-macros [enfocus.macros :as em]
                   [cljs.core.async.macros :refer [go go-loop]]))


(em/defsnippet image-link-attachment-edit :compiled "templates/blog.html" [".attachment__image-link-edit"]
  [ch link]

  ".attachment__image-link-edit"
    (events/listen :focusout
       (fn [e]
         (.log js/console "focusout!")
         (close! ch)))

  ".attachment__image-link-edit__link-input"
    (ef/set-attr :value link)

  ".attachment__image-link-edit__confirm"
    (events/listen :mousedown
      (fn [e]
        (.preventDefault e)
        (.log js/console "mousedown")
        (let [link (clojure.string/trim (ef/from ".attachment__image-link-edit__link-input" (ef/get-prop :value)))]
          (when (image-ext? link)
            (put! ch {:type :image :url link}))))))


(defmethod attachment-edit :image-link [type_ ch event link]
  (.log js/console "image attachment!")
  (image-link-attachment-edit ch link))


