(ns terekheta.attachments.core-templates
  (:require [enfocus.core :as ef]
            [enfocus.events :as events]
            [cljs.core.async :refer [>! <! chan close! put! take!]])
  (:require-macros [enfocus.macros :as em]))


(em/defsnippet attachment-edit-preview :compiled "templates/blog.html" [".attachment-edit__preview"]
  [ch content]

  ".attachment-edit__preview__content"
    (ef/content content)

  ".attachment__delete"
    (events/listen :click
      (fn [e]
        (.preventDefault e)
        (close! ch))))
