(ns terekheta.attachments.image-core
  (:require [enfocus.core :as ef]
            [enfocus.events :as events]

            [terekheta.attachments.core :refer [attachment-preview]])
  (:require-macros [enfocus.macros :as em]))


(def image-ext-regex (js/RegExp. "\\.jpg|jpeg|png|gif|bmp$" "i"))
(def image-type-regex (js/RegExp. "^image/"))

(defn image-ext? [img?]
  (.test image-ext-regex img?))

(defn image-type? [img?]
  (.test image-type-regex (.-type img?)))

(image-type? (js-obj "type" "image/"))

(em/defsnippet image-attachment-preview :compiled "templates/blog.html" [".attachment__image-preview"]
  [att]

  ".attachment__image-preview"
    (ef/replace-vars att))


(defmethod attachment-preview :image [att]
  (image-attachment-preview att))
