(ns terekheta.utils.send-form
  (:require [ajax.core :refer [POST PUT]]
            [enfocus.core :as ef]))

(defn send-form-as-data [form & [append-data on-success on-error]]  ;;
   (let [post-values (merge (ef/at form (ef/read-form))
                            (or append-data {}))
         send* (if (= (post-values :_method) "PUT") PUT POST)
         url (.-action form)]
     (.log js/console (pr-str post-values))
     (send* url {:params post-values
                 :headers {"x-csrf-token" (or (post-values :__anti-forgery-token)
                                              (-> js/document (.querySelector "[name=csrf-token]") .-content))}
                 :handler on-success
                 :error-handler on-error})))
