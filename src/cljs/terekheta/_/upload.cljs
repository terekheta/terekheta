(ns terekheta._.upload
  (:require [cljs.core.async :refer [>! <! chan close! put! take!]]
            [ajax.core :refer [POST]]
            [goog.events :as gev]
            [goog.dom :as gdom]))

(defn upload [file url response-ch progress-ch]
  (let [xhr (js/XMLHttpRequest.)
;;         (.querySelector document "name=csrf-token")
        form-data (doto
                    (js/FormData.)
                    (.append "id" "10")
                    (.append "file" file)
                    (.append "__anti-forgery-token" (-> js/document (.querySelector "[name=csrf-token]") .-content))
                    )]
    (gev/listen xhr "load"
      (fn [e]
        (when (and
               (= (.-status xhr) 200)
               (= (.-statusText xhr) "OK"))
          (put! response-ch (.-response xhr))
          (close! response-ch))))
    (gev/listen xhr "error" #(close! response-ch))
    (gev/listen (-> xhr .-upload) "progress"
      (fn [e]
        (when-let [progress (/ (.-loaded e) (.-total e))]
          (put! progress-ch progress))))
    (.open xhr "POST" url true)
    (.send xhr form-data)))

;; (defn upload [file url success-ch error-ch progress-ch]
;;   (let [form-data (doto
;;                     (js/FormData.)
;;                     (.append "id" "10")
;; ;;                     (.append "file" file "filename.txt")
;;                     )]
;;     (POST url {:params form-data
;; ;;                :response-format (raw-response-format)
;;                :timeout 100})))


;; (upload ["123" "posts/upload"])
