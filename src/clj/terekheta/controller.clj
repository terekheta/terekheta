(ns terekheta.controller
  (:refer-clojure :exclude (list delete))
  (:use compojure.core))

(defprotocol RESTController
  (list [_])
  (show [_ id])
  (create [this])
  (new [_])
  (edit [_ id])
  (update [_ id])
  (delete [_ id]))
