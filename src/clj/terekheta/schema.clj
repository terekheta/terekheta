(ns terekheta.schema)


(defonce schema
  {:account {:name             [{:required true}]
             :email            [{:required true, :unique :value}]
             :role             [{:type :keyword, :required true}]
             :password-digest  [{:required true}]
             :created-at       [{:type :instant}]
             :updated-at       [{:type :instant}]}
   :post   {:title       [{:required true, :fulltext true}]
            :author      [{:type :ref, :ref  {:ns :account}}]
            :content     [{:fulltext true}]
            :comments    [{:type :ref, :ref {:ns :comment}, :cardinality :many, :isComponent true}]
            :attachments [{:type :ref, :ref {:ns :attachment}, :cardinality :many, :isComponent true}]
            :published?  [{:type :boolean, :default false}]
            :created-at  [{:type :instant}]
            :updated-at  [{:type :instant}]}
   :comment {:author      [{:required true, :type :ref, :ref  {:ns :account}}]
             :content     [{}]
             :comments    [{:type :ref, :ref {:ns :comment}, :cardinality :many, :isComponent true}]
             :attachments [{:type :ref, :ref {:ns :attachment}, :cardinality :many, :isComponent true}]
             :published?  [{:type :boolean, :default true}]
             :created-at  [{:type :instant}]
             :updated-at  [{:type :instant}]}
   :attachment {:url  [{:type :uri}]
                :id   [{}]
                :type [{:type :enum,
                        :required true
                        :enum {:ns :attachment.type, :values #{:youtube-video :image}}}]}})

;; (defonce url "datomic:free://localhost:4334/terekheta")
;; (def ds (adi/connect! url schema false false))

;; (adi/select ds :account)

;; (def add-attachments
;;   {:post {:attachments [{:type :ref, :ref {:ns :attachment}, :cardinality :many, :isComponent true}]}
;;    :comment {:attachments [{:type :ref, :ref {:ns :attachment}, :cardinality :many, :isComponent true}]}})

;; (def add-att-id {:attachment {:id [{}]}})
