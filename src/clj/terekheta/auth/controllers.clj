(ns terekheta.auth.controllers
  (:require [clojure.string :as str]
            [ring.util.response :as ring-response]
            [cemerick.friend :as friend :only [logout*]]

            [terekheta.auth.views :as views :only [login]]
            [terekheta.auth.queries :as aq]
            [terekheta.auth.roles :as ar]
            [terekheta.auth.utils :as au]))


(defn login []
  (views/login))


(defn logout [url]
  (friend/logout* (ring-response/redirect url)))


(defn signup
  ([]
   (views/signup))
  ([{:keys [name username password] :as params}]
   (if (not-any? str/blank? [name username password])
     (do
       (aq/create-account name username password ::ar/blogger)
       (friend/merge-authentication
        (ring-response/redirect "/")
        (au/user-credentials username)))
     (assoc (ring-response/redirect (str "/signup")) :flash "cant be blank!"))))
