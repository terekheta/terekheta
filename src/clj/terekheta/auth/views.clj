(ns terekheta.auth.views
  (:require [net.cgrand.enlive-html :as html]
            [terekheta.auth.utils :refer [current-user user-is-authenticated]]
            [terekheta._.views :refer [layout]]))


(html/defsnippet login-form "templates/auth.html" [:.login]
  [])


(html/defsnippet signup-form "templates/auth.html" [:.signup]
  [])


(defn login []
  (let [main (html/content (if-not (user-is-authenticated)
                             (login-form)
                             (html/html-snippet "You have already logged in!")))]
    (layout {:main main})))

(defn signup []
  (let [main (html/content (if-not (user-is-authenticated)
                             (signup-form)
                             (html/html-snippet "You have already logged in!")))]
    (layout {:main main})))
