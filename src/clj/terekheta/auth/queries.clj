(ns terekheta.auth.queries
  (:require [datomic.api :as d]
            [adi.core :as adi]
            [cemerick.friend.credentials :refer (hash-bcrypt)]

            [utils.utils :refer [deep-merge]]

            [terekheta._.queries :refer [create-mixin update-mixin]]
            [terekheta.db :as db]
            [terekheta.auth.roles :as ar]))


(defn create-account [name- email password role]
  (adi/insert! db/adi-conn (deep-merge {:account {:name name-
                                                  :email email
                                                  :password-digest (hash-bcrypt password)
                                                  :role role}}
                                       (create-mixin :account))))


(defn get-account-by-email [email]
;;   (if-let [aid (ffirst (d/q '[:find ?a :in $ ?email :where [?a :account/email ?email]] db/*db* email))]
;;     (db/get-entity-by-id db/*db* aid))
  (first (adi/select db/adi-conn {:account {:email email}} :ids)))

;; @(d/transact db/connection [{:db/id (:db/id (get-account-by-email "ubivaza@gmail.com")) :account/role ::ar/admin}])

(defn account-list []
  (adi/select db/adi-conn :account :ids))

;; (account-list)
