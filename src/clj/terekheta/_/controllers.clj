(ns terekheta._.controllers
  (:require [clojure.string :as str]
            [ring.util.response :as ring-response]
            [cemerick.friend :as friend :only [logout*]]

            [terekheta._.views :as _v :only [index]]
;;             [terekheta._.queries :as aq]
            [terekheta.auth.roles :as ar]))


(defn index []
  (_v/index))
