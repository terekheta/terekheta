(ns terekheta._.views
  (:require [net.cgrand.enlive-html :as html]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]

            [terekheta.auth.utils :refer [current-user user-is-authenticated]]))



(html/defsnippet jumbotron "templates/_.html" [:.header__jumbotron]
  [])


(html/defsnippet navbar "templates/_.html" [:.header__navbar]
  [])


(html/defsnippet meta-csrf "templates/_.html" [[:meta (html/attr= :name "csrf-token")]]
  []
  [:meta] (html/set-attr :content *anti-forgery-token*))


(html/deftemplate layout "templates/layout.html"
  [{:keys [main footer-scripts]}]
  [:body :> :main] main
  [:body :> :header] (html/content (when (user-is-authenticated)
                                  (navbar)))
  [:body] (html/append footer-scripts)
  [:form] (html/append (html/html-snippet (anti-forgery-field)))
  [:head] (html/append (meta-csrf)))


(defn index []
  (let [main (html/content
              (when-not (user-is-authenticated)
                (jumbotron)))]
    (layout {:main main})))
