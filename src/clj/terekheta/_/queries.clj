(ns terekheta._.queries
  (:require [adi.core :as adi]))

(defn create-mixin [doc]
  {doc {:created-at (java.util.Date.)
             :updated-at (java.util.Date.)}})

(defn update-mixin [doc]
  {doc {:updated-at (java.util.Date.)}})
