(ns terekheta.resource
  (:use compojure.core))


(defmacro resource [path c]
  `(context ~path []
            (GET   "/" [] (.list ~c))
            (POST  "/" [] (.create ~c))
            (GET   "/new" [] (.new ~c))
            (context ["/:id", :id #"[0-9]+"] [~'id]
                     (GET "/" [] (.show ~c (Long/parseLong ~'id)))
                     (PUT "/" [] (.update ~c (Long/parseLong ~'id)))
                     (DELETE "/" [] (.delete ~c (Long/parseLong ~'id)))
                     (GET "/edit" [] (.edit ~c (Long/parseLong ~'id))))))


;; --- tests ---
;; (macroexpand-1 '(resourse "asd" 1))
;; (macroexpand-1 '(GET "/" [] (.show ~c (Long/parseLong id))))
