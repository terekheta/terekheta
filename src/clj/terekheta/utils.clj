(ns terekheta.utils
  (:require [terekheta.db :as db]))

(def ^:dynamic *request* nil)

(defn bind-request [handler]
  (fn [request]
    (binding [*request* request]
      (handler request))))

(defn bind-db [handler]
  (fn [request]
    (binding [db/*db* (db/get-db)]
      (handler request))))
