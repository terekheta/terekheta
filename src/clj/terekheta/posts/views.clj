(ns terekheta.posts.views
  (:require [clojure.algo.generic.functor :refer [fmap]]
            [net.cgrand.enlive-html :as html]
            [utils.utils :refer [->transit]]

            [terekheta.helpers :refer [stringify]]
            [terekheta._.views :refer [layout]]

            [terekheta.auth.roles :as ar]
            [terekheta.auth.utils :as au]
            [terekheta.posts.utils :as pu]

            [terekheta.comments.views :refer [comments comment-edit-form]]
            [terekheta.comments.helpers :refer [new-comment-stub]]))



;; (html/defsnippet youtube-content "templates/_.html" [:.ytplayer]
;;   [video]
;;   [html/any-node] (html/replace-vars video))

;; (defn update-values [m f & args]
;;   (reduce (fn [r [k v]] (assoc r k (apply f v args))) {} m))

(defmacro defsnippet-replace-vars
  [name source selector]
  `(html/defsnippet ~name ~source ~selector [~'vars]
     [html/any-node] (html/replace-vars (fmap str ~'vars))))

;; (macroexpand-1 '(defsnippet-replace-vars youtube-video-attachment-preview "templates/blog.html" [:.attachment__youtube-video-preview]))

(defsnippet-replace-vars youtube-video-attachment-preview "templates/blog.html" [:.attachment__youtube-video-preview])
(defsnippet-replace-vars image-attachment-preview "templates/blog.html" [:.attachment__image-preview])

(defmulti attachment-preview (fn [att] (att :type)))

(defmethod attachment-preview :default [att]
  ;; TODO
  )

(defmethod attachment-preview :youtube-video [att]
  (youtube-video-attachment-preview att))

(defmethod attachment-preview :image [att]
  (image-attachment-preview att))



;; (attachment-preview {:type :youtube-video :id 123})

(defn- render-attachments [atts]
;;   (println (reduce conj "" (map attachment-preview (post :attachments))))
  (fmap attachment-preview atts))

;; (render-post-attachments {:attachments [{:type :youtube-video :id "x0RrwbzziWQ"} {:type :image :url "http://habrastorage.org/files/044/ff8/5d7/044ff85d733048ae92f406b038f9dd2c.png"}]})

(defn- prepare-post-content [post]
  (clojure.string/replace (:content post) #"\n" {"\n" "<br>"}))

(html/defsnippet post-content "templates/blog.html" [:.post]
  [post]
  [:.post :.post__content]
    (html/html-content (prepare-post-content post))
  [:.post html/any-node]
    (html/replace-vars (fmap stringify post))
  [:.post :.post__attachments] (html/content (render-attachments (:attachments post))))


(html/defsnippet post-edit-form "templates/blog.html" [:.post-edit]
  [post is-new?]
  [html/any-node] (html/replace-vars (fmap stringify post))
  [:form] (if-not is-new?
            (html/append (html/html-snippet "<input type='hidden' name='_method' value='PUT'>"))
            identity))


(html/defsnippet blog-content "templates/blog.html" [:.blog]
  [posts]
  [:.blog [:.blog__post html/first-of-type]]
    (html/clone-for [post posts]
      [:.blog__post :.blog__post__content]
        (html/html-content (prepare-post-content post))
      [:.blog__post html/any-node] (html/replace-vars (fmap stringify post))
      [:.blog__post :.blog__post__controls] (when (pu/user-can-manage-post? post) identity))

  ;; if there are any posts - remove by returning nil
  [:.blog__no-posts] (when (empty? posts) identity)
  [:.blog__new] (when (au/user-has-role ::ar/blogger) identity))


(html/defsnippet blog-footer-scripts "templates/blog.html" [[:script (html/attr? :src)]] [])

(html/defsnippet post-edit-bootstrap-script "templates/blog.html" [:#post-edit-bootstrap-script]
  [post]

  [html/any-node] (html/replace-vars {:post-bootstrap (pr-str (->transit post))}))


(defn blog-page [posts]
  (layout {:main (html/content (blog-content posts))
           :footer-scripts (blog-footer-scripts)}))


(defn post-page [post]
  (layout {:main (html/append
                  (post-content post)
                  (comments (:comments post))
                  (html/at (comment-edit-form new-comment-stub true)
                    [:form] (html/set-attr :action (str "/posts/" (:id post) "/comment")))
                  )}))

;; (apply str (html/html-snippet (comment-edit-form new-comment-stub true)
;;                    [] ()))


(defn post-edit-page [post]
  (layout {:main (html/content (post-edit-form post false))
           :footer-scripts [(blog-footer-scripts)
                            (post-edit-bootstrap-script post)]}))

(defn post-new-page []
  (layout {:main (html/content (post-edit-form pu/new-post-stub true))
           :footer-scripts (blog-footer-scripts)}))


