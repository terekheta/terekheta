(ns terekheta.posts.utils
  (:require [terekheta.db :as db]
            [terekheta.auth.utils :as au]
            [terekheta.auth.roles :as ar])
  (:use [clojure.set]))


(defn user-can-manage-post? [post]
  (let [current-user-id (:db/id (au/current-user))
        post-author-id (:db/id (:post/author post))]
    (or (au/user-has-role ::ar/admin)
        (and (au/user-has-role ::ar/blogger)'
             (= current-user-id post-author-id)))))



(def new-post-stub
  {:id ""
   :title ""
   :content ""})
