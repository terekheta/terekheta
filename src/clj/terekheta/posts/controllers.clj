(ns terekheta.posts.controllers
  (:require [terekheta.db :as db]
            [terekheta.utils :refer [*request*]]
            [terekheta.helpers :refer [require-params permit-params]]

            [terekheta.helpers :refer [merge-id deep-merge-id]]

            [terekheta.auth.utils :as au]
            [terekheta.auth.roles :as ar]

            [terekheta.posts.utils :as pu]
            [terekheta.posts.queries :as pq]
            [terekheta.posts.views :as pv]

            [terekheta.controller :refer (RESTController)])
  (:use [ring.util.response]
        [clojure.set]))

;; (deep-merge-id (pq/get-post 17592186045640))
;; (update-in (deep-merge-id (pq/get-post 17592186045640)) [:content] #(clojure.string/escape % {\n "<br>"}))
;; (def post (first (pq/post-list)))
;; (def merged-post (merge-id post))

;; (:author merged-post)
;; (pv/blog-page (mapv merge-id (pq/post-list)))
;; (pv/post-page (first (pq/post-list)))
;; (pq/post-list)
;; (pq/get-post 17592186045640)
;; (merge-id (pq/get-post 17592186045640))

;; (merge-id (pq/get-post 17592186045640))
;; (deep-merge-id (pq/get-post 17592186045640))
;; (pv/post-edit-bootstrap-script (merge-id (pq/get-post 17592186045640)))

(defn check-permissions

  ([]
   (cond
    (not au/user-is-authenticated)        (throw (Exception. "Login first!"))
    (not (au/user-has-role ::ar/blogger)) (throw (Exception. "not enough rights"))))

  ([id]
   (check-permissions)

   (let [post (db/get-entity-by-id id)]
     (cond
      (not post)                            (throw (Exception. "no such post"))
      (not (pu/user-can-manage-post? post)) (throw (Exception. "not enough rights"))))))

(deftype PostController []
  RESTController

  (list [_]
    (check-permissions)
    ;; TODO sort
    (pv/blog-page (mapv deep-merge-id (pq/post-list))))

  (show [_ id]
    (check-permissions)
    (pv/post-page (deep-merge-id (pq/get-post id))))

  (create [_]
    (print "CREATE POST!" (:params *request*))
    (check-permissions)
    (let [post-data (-> (:params *request*)
                      (permit-params [:title :content])
                      (assoc :author (:id (au/current-user))))
          new-post (merge-id (pq/create-post {:post post-data}))]
      "OK"
;;         (redirect (str "/posts/" (:id new-post)))
      ))

  (new [_]
    (check-permissions)
    (pv/post-new-page))

  (edit [_ id]
    (check-permissions id)
    (pv/post-edit-page (deep-merge-id (pq/get-post id))))

  (update [_ id]
    (println "HELLO POST!" id *request*)
    (check-permissions id)
    (let [post-data (permit-params (:params *request*) [:title :content :attachments])]
      (println (:params *request*) post-data)
      (pq/update-post id {:post post-data})
      "OK")
;;     (redirect (str "/posts/" id))
          )

  (delete [_ id]
    (check-permissions id)
    (db/destroy-entity id)
    (redirect "/posts/")))


(defn comment-post [id]
  (check-permissions id)
  (let [comment-data (-> (:params *request*)
                       (permit-params [:content])
                       (assoc :author (:id (au/current-user))))]
    (println comment-data)
    (pq/comment-post id comment-data)
    (redirect (str "/posts/" id))))



(def post-controller (PostController.))
