(ns terekheta.posts.queries
  (:require [adi.core :as adi]

            [utils.utils :refer [deep-merge]]

            [terekheta.db :as db]
            [terekheta._.queries :refer [create-mixin update-mixin]]
            [terekheta.comments.queries :refer [create-comment]]))



;; (defn- db-update-post [data]
;;   @(d/transact db/connection [(assoc data :post/updated-at (java.util.Date.))]))

;; (defn- db-create-post [data]
;;   (let [post-id (d/tempid :db.part/user)
;;         res (db-update-post (merge {:post/published? false
;;                                     :post/created-at (java.util.Date.)}
;;                                    data
;;                                    {:db/id post-id}))]
;;     (d/resolve-tempid (:db-after res) (:tempids res) post-id)))

(defn get-post [id]
  (first (adi/select db/adi-conn id :ids
              :access {:post {:author :checked
                              :comments {:author {:name :checked
                                                  :password-digest :unchecked}}
                              :attachments :checked}})))

(defn post-list []
  (adi/select db/adi-conn :post :ids
              :access {:post {:author :checked}}))

;; (post-list)

(defn create-post

  ;;   ([title author-id content]
  ;;    (create-post {:post/title title
  ;;                  :post/author author-id
  ;;                  :post/content content}))

  ([data]
   (adi/insert! db/adi-conn (deep-merge data (create-mixin :post)))))

(defn update-post [id data]
  (adi/sync-> db/adi-conn
    (adi/delete-in! id [:post/attachments {}])
    (adi/update! id (deep-merge data (update-mixin :post)))))

;; (create-post "asd" 17592186045418 "asd")
;; (adi/select db/adi-conn 17592186045640 :access {:post {:attachments :checked}})
;; (update-post 17592186045640 {:post {:content 123}})
;; (get-post 17592186045640

(defn comment-post [post-id comment-data]
  (let [comm (create-comment {:comment comment-data})
        comm-id (get-in comm [:db :id])]
    (adi/update! db/adi-conn
                 post-id
                 {:post/comments comm-id})))

;; (comment-post 17592186045640 {:comment {:content "kuku!" :author (get-in (first (post-list)) [:post :author])}})

;; author 17592186045589
;; comment 17592186045866
;; post 17592186045640
