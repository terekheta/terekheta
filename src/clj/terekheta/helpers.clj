(ns terekheta.helpers
  (:require [terekheta.db :as db]
            [clojure.algo.generic.functor :refer [fmap]]))

(defn require-params [data params]
  (doseq [p params]
    (when
      (and (contains? data p)
           (contains? data (keyword p)))
      (throw (Exception. (str "request should contain " p " parameter")))))
  data)


(defn permit-params [data params]
  (into {}
        (for [[k v] data
              :let [kk (keyword k)]
              :when (some #(or (= kk %) (= k %)) params)]
          [kk v])))


(defn stringify [v]
  (if (keyword? v)
    (name v)
    (str v)))


(defn merge-id [doc]
  (->> doc vals (apply merge)))


(defn deep-merge-vals-id [vs]
  (cond
    (and (map? vs) (contains? vs :+)) ;; проверить нет ли ключа +, если есть - слить
      (deep-merge-vals-id (merge (get-in vs [:+ :db]) (dissoc vs :+)))
    (coll? vs)  ;; для словарей тоже работает, которые не попали в первое условие
      (fmap deep-merge-vals-id vs)  ;; пройтись по значениям
    :else vs))

(defn deep-merge-id [doc]
  (-> doc merge-id deep-merge-vals-id))
