(ns terekheta.comments.views
  (:require [clojure.algo.generic.functor :refer [fmap]]
            [net.cgrand.enlive-html :as html]
            [terekheta.helpers :refer [stringify]]

            [terekheta.auth.roles :as ar]
            [terekheta.auth.utils :as au]

            [terekheta.comments.helpers :refer [new-comment-stub]]))


;; (html/defsnippet comments-content "templates/comments.html" [:.comment]
;;   [comments]
;;   [html/any-node] (html/replace-vars (merge (pu/prepare-post-data post) new-comment-stub)))


;; (html/defsnippet post-edit-form "templates/blog.html" [:.post-edit]
;;   [post is-new?]
;;   [html/any-node] (html/replace-vars (pu/prepare-post-data post))
;;   [:form] (if-not is-new?
;;             (html/append (html/html-snippet "<input type='hidden' name='_method' value='PUT'>"))
;;             identity))


(html/defsnippet comment-edit-form "templates/comments.html" [:.comment-edit]
  [com is-new?]
  [:.comment-edit html/any-node] (html/replace-vars (fmap stringify com))
  [:.comment-edit :form] (if-not is-new?
            (html/append (html/html-snippet "<input type='hidden' name='_method' value='PUT'>"))
            identity))


(html/defsnippet comments "templates/comments.html" [:.comments]
  [comments]
  [:.comments [:.comments__comment html/first-of-type]]
    (html/clone-for [com (sort-by :created-at comments)]
      [:.comments__comment__author html/any-node] (html/replace-vars (:author com))
      [html/any-node] (html/replace-vars (fmap stringify com))))



