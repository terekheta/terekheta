(ns terekheta.comments.helpers
  (:require [datomic.api :as d]
            [terekheta.db :as db]))

(def new-comment-stub
  {:content ""})
