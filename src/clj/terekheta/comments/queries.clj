(ns terekheta.comments.queries
  (:require [datomic.api :as d]
            [adi.core :as adi]

            [utils.utils :refer [deep-merge]]

            [terekheta._.queries :refer [create-mixin update-mixin]]
            [terekheta.db :as db]))

(defn create-comment [data]
  (adi/insert! db/adi-conn (deep-merge data (create-mixin :comment))))
