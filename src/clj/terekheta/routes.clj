(ns terekheta.routes
  (:require [clojure.java.io :as io]

            [com.akolov.enlive-reload :refer [wrap-enlive-reload]]
            [compojure.route :as route]
            [clojure.java.io :as io]
            [ring.middleware.defaults :refer :all]
;;             [ring.middleware.multipart-params :refer [wrap-multipart-params]]

            [cemerick.friend :as friend]
            (cemerick.friend [workflows :as workflows]
                             [credentials :as creds])

            [terekheta.utils :refer [bind-db bind-request *request*]]
            [terekheta.resource :as rs]
            [terekheta.posts.controllers :as pc]
            [terekheta.auth.controllers :as ac]
            [terekheta.auth.roles :as ar]
            [terekheta._.controllers :as _c]
            [terekheta.auth.utils :as au])
  (:use compojure.core
        compojure.handler
        terekheta.posts.controllers
        [ring.util.response :only [response]]
        [ring.middleware.transit :only [wrap-transit-params wrap-transit-response]]))


(defn role-required [role handler]
  (fn [request]
    (if (au/user-has-role role)
      (handler request)
      (throw (Exception. "not enough rights")))))

;; (-> {:posts {:1 :p1}} :posts :1)

(System/getProperty "file.encoding")

(defn- upload [params]
;;   (clojure.pprint/pprint params)
  (let [{{:keys [tempfile filename]} :file} params]
    (io/copy tempfile (io/file "resources" "public" "upload" filename))
    (response {:url (str "/upload/" filename)
               :type :image})))

(defroutes compojure-handler
  (GET "/" [] (_c/index))  ;; (pr-str *request* (friend/current-authentication))
  ;;   (GET "/transit" [] (response {:data 123}))
;;   (resource "/posts" pc)

  (wrap-transit-response
    (POST "/upload" {:keys [params]} (upload params)))

  (POST ["/posts/:id/comment", :id #"[0-9]+"] [id] (pc/comment-post (Long/parseLong id)))
  (rs/resource "/posts" pc/post-controller)

  (GET "/login" [] (ac/login))
  (GET "/signup" [] (ac/signup))
  (POST "/signup" {params :params}  (ac/signup params))
  (POST "/logout" [] (ac/logout "/"))




  (ANY "/req" request (str request))
  (route/resources "/")
  ;;   (route/files "/" {:root (config :external-resources)})
  (route/not-found "Not found!")



  )

(defn wrap-auth [routes]
  (friend/authenticate routes {:allow-anon? true
                               :login-uri "/login"
                               :default-landing-uri "/"
;;                                :unauthorized-handler #(-> ("<h2> You do not have sufficient privileges to access this page")
;;                                                            ring-response/response (ring-response/status 401))
                               :credential-fn (partial creds/bcrypt-credential-fn au/user-credentials)
                               :workflows [(workflows/interactive-form)]}))


;; todo if not logged in - redirect
(defn wrap-exception [f]
  (fn [request]
    (try (f request)
      (catch Exception e
         {:status 500
          :body "Exception caught"})))) ;;"Exception caught" (pr-str request)

(def app
  (-> compojure-handler
      bind-request
;;       wrap-auth  ;; comment in dev
      (wrap-defaults site-defaults)  ;; (assoc-in site-defaults [:security :anti-forgery] false)
;;       wrap-exception  ;; comment in dev

      wrap-transit-params
      bind-db

      ;; only dev
      (ring.middleware.reload/wrap-reload)
      (wrap-enlive-reload)

      ))
