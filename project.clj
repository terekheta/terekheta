(defproject terekheta "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :source-paths ["src/clj"]
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [;; nrepl
;;                  [lein-light-nrepl "0.0.19"]
                 [com.akolov.enlive-reload "0.1.0"]

                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]

                 ;; clj
                 [org.clojure/clojure "1.6.0"]
                 [org.clojure/algo.generic "0.1.2"]
                 [com.cognitect/transit-clj "0.8.259"]
                 [com.datomic/datomic-free "0.9.4956"]
                 [im.chit/adi "0.3.1-SNAPSHOT"]  ;; datomic interface
                 [ring/ring-anti-forgery "1.0.0"]
                 [ring/ring-defaults "0.1.2"]
                 [ring-transit "0.1.2"]
                 [compojure "1.2.0"]
                 [com.cemerick/friend "0.2.1"]  ;; auth
                 [enlive "1.1.5"]

                 ;; cljs
                 [org.clojure/clojurescript "0.0-2371"]
                 [com.cognitect/transit-cljs "0.8.192"]
                 [enfocus "2.1.1"]
                 [cljs-ajax "0.3.3"]
                 [jayq "2.5.2"]]


;;   :repl-options {:nrepl-middleware [lighttable.nrepl.handler/lighttable-ops]}

  :plugins [[lein-cljsbuild "1.0.3"]
            [lein-ring "0.8.13"]]
;;   :hooks [leiningen.cljsbuild]
  :cljsbuild {:builds {
               :_
               {:source-paths ["src/cljs/_"]
                :compiler {:output-to "resources/public/js/_.js"
;;                            :output-dir "resources/public/js/_/"
                           :externs ["resources/public/js/externs/jquery-2.1.1.min.js"]
                           :optimizations :whitespace
;;                            :source-map "resources/public/js/_.js.map"
                           :pretty-print true}}

               :posts
               {:source-paths ["src/cljs/terekheta"]
                :compiler {:output-to "resources/public/js/posts.js"
;;                            :output-dir "resources/public/js/posts/"
;;                            :externs ["resources/public/js/externs/jquery-2.1.1.min.js"]
                           :optimizations :whitespace
;;                            :source-map "resources/public/js/posts.js.map"
                           :pretty-print true}}
;;                :attachments
;;                {:source-paths ["src/cljs/attachments"]
;;                 :compiler {:output-to "resources/public/js/attachments.js"
;; ;;                            :output-dir "resources/public/js/posts/"
;; ;;                            :externs ["resources/public/js/externs/jquery-2.1.1.min.js"]
;;                            :optimizations :whitespace
;; ;;                            :source-map "resources/public/js/posts.js.map"
;;                            :pretty-print true}}

;;                {:id "release"
;;                         :source-paths ["src/cljs"]
;;                         :compiler {:output-to "resources/public/js/app.js"
;;                                    :optimizations :advanced
;;                                    :pretty-print false
;;                                    :output-wrapper false
;;                                    :closure-warnings {:non-standard-jsdoc :off}}}
                       }}

  :ring {:handler terekheta.routes/app})
